var rulesShown = false;

/*var ranks = [
    "Bronze", "Silver V", "Silver IV", "Silver III", "Silver II", "Silver I", "Gold V", "Gold IV", "Gold III", "Gold II",
    "Gold I", "Platinum V", "Platinum IV", "Platinum III", "Platinum II", "Platinum I", "Diamond V", "Diamond IV",
    "Diamond III", "Diamond II", "Diamond I", "Crown V", "Crown IV", "Crown III", "Crown II", "Crown I", "Ace"
];*/
var ranks = [
    "Bronze",
    "Silver",
    "Gold",
    "Platinum",
    "Diamond",
    "Master/Heroic"
];
var prices = [
    [100000, 150000, 175000, 200000, 250000, 300000, 350000, 400000, 450000, 500000, 550000, 650000, 725000, 800000, 875000, 950000],
    [0, 50000, 75000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 550000, 625000, 700000, 775000, 825000],
    [0, 0, 0, 0, 50000, 100000, 150000, 200000, 250000, 300000, 350000, 450000, 525000, 600000, 675000, 700000],
    [0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 250000, 300000, 400000, 450000, 500000, 550000, 650000],
    [0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 250000, 350000, 400000, 450000, 525000, 600000],
    [0, 0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 300000, 350000, 425000, 500000, 550000],
    [0, 0, 0, 0, 0, 0, 0, 0, 50000, 125000, 200000, 250000, 325000, 400000, 475000, 525000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 125000, 200000, 300000, 375000, 425000, 500000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 225000, 300000, 375000, 400000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 125000, 175000, 300000, 325000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 250000, 300000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 200000, 250000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 200000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 175000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
];

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    for (var i=0; i<ranks.length; i++) {
        $("#current-ranks").append("<option>"+ranks[i]+"</option>");
    }
    for (var i=1; i<ranks.length; i++) {
        $("#target-ranks").append("<option>"+ranks[i]+"</option>");
    }
    $("#current-ranks").change(function() {
        refreshPrice();
    });
    $("#target-ranks").change(function() {
        refreshPrice();
    });
    $("#current-tiers").change(function() {
        refreshPrice();
    });
    $("#target-tiers").change(function() {
        refreshPrice();
    });
});

function showRules() {
    rulesShown = !rulesShown;
    if (rulesShown) {
        $("#dropdown-img").css({
            "transform": "rotate(-180deg)"
        });
        $("#rules").css({
            "height": "470px"
        });
    } else {
        $("#dropdown-img").css({
            "transform": "rotate(0deg)"
        });
        $("#rules").css({
            "height": "100px"
        });
    }
}

function checkout() {
    var currentRank = $("#current-ranks").val();
    var currentTier = $("#current-tiers").val();
    //var currentStar = $("#current-stars").val();
    var targetRank = $("#target-ranks").val();
    var targetTier = $("#target-tiers").val();
    //var targetStar = $("#target-stars").val();
    var server = 0;
    if ($("#ios").prop("checked")) {
        server = 1;
    }
    var level = $("#level").val().trim();
    var heros = $("#heros").val().trim();
    var email = $("#email").val().trim();
    var password = $("#password").val();
    if (level == "") {
        alert("Mohon masukkan level");
        return;
    }
    if (heros == "") {
        alert("Mohon masukkan jumlah hero");
        return;
    }
    if (email == "") {
        alert("Mohon masukkan email Moonton");
        return;
    }
    if (password == "") {
        alert("Mohon masukkan password Moonton");
        return;
    }
    var agree = $("#checkbox").prop("checked");
    if (!agree) {
        alert("Mohon setujui syarat dan ketentuan");
        return;
    }
    refreshPrice();
    $("#checkout").prop("disabled", true);
    var fd = new FormData();
    fd.append("type", "Free Fire Mobile");
    fd.append("current_rank", currentRank);
    fd.append("current_tier", currentTier);
    fd.append("target_rank", targetRank);
    fd.append("target_tier", targetTier);
    fd.append("server", server);
    fd.append("level", level);
    fd.append("heros", heros);
    fd.append("email", email);
    fd.append("password", password);
    $.ajax({
        type: 'POST',
        url: 'https://fdelivery.xyz/joki-mobile-legends/php/checkout.php',
        data: fd,
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
            alert("Pesanan selesai dibuat, kamu akan menerima email jika pesanan sudah dilakukan.");
            $("#current-ranks").prop("selectedIndex", 0);
            $("#current-tiers").prop("selectedIndex", 0);
            $("#target-ranks").prop("selectedIndex", 0);
            $("#target-tiers").prop("selectedIndex", 0);
            $("#level").val("");
            $("#heros").val("");
            $("#email").val("");
            $("#password").val("");
            $("#agree").prop("checked", false);
            $("#android").prop("checked", true);
            $("#checkout").prop("disabled", false);
            window.location.href = "http://wa.me/081280792311";
        }
    });
}

function refreshPrice() {
    var currentRank = $("#current-ranks").prop("selectedIndex");
    var currentTier = $("#current-tiers").prop("selectedIndex");
    var targetRank = $("#target-ranks").prop("selectedIndex");
    var targetTier = $("#target-tiers").prop("selectedIndex");
    var row = -1;
    if (currentRank == 0) {
        row = 0;
    } else if (currentRank == 1) {
        row = 1;
    } else if (currentRank == 2) {
        switch (currentTier) {
            case 0: row = 2; break;
            case 1: row = 3; break;
            case 2: row = 4; break;
            case 3: row = 5; break;
        }
    } else if (currentRank == 3) {
        switch (currentTier) {
            case 0: row = 6; break;
            case 1: row = 7; break;
            case 2: row = 8; break;
            case 3: row = 9; break;
        }
    } else if (currentRank == 4) {
        switch (currentTier) {
            case 0: row = 10; break;
            case 1: row = 11; break;
            case 2: row = 12; break;
            case 3: row = 13; break;
        }
    } else if (currentRank == 5) {
        row = 14;
    }
    var col = -1;
    if (targetRank == 0) {
        switch (targetTier) {
            case 0: col = 0; break;
            case 1: col = 1; break;
            case 2: col = 2; break;
        }
    } else if (targetRank == 1) {
        switch (targetTier) {
            case 0: col = 3; break;
            case 1: col = 4; break;
            case 2: col = 5; break;
            case 3: col = 6; break;
        }
    } else if (targetRank == 2) {
        switch (targetTier) {
            case 0: col = 7; break;
            case 1: col = 8; break;
            case 2: col = 9; break;
            case 3: col = 10; break;
        }
    } else if (targetRank == 3) {
        switch (targetTier) {
            case 0: col = 11; break;
            case 1: col = 12; break;
            case 2: col = 13; break;
            case 3: col = 14; break;
        }
    } else if (targetRank == 4) {
        col = 15;
    }
    console.log("Current rank: "+currentRank+", current tier: "+currentTier+", target rank: "+targetRank+", target tier: "+targetTier+", row: "+row+", col: "+col);
    var price;
    if (row == -1 || col == -1) {
        price = 0;
    } else {
        price = prices[row][col];
    }
    console.log("Price = "+price);
    $("#price").html("Rp"+formatMoney(price)+",-");
}