var rulesShown = false;

var ranks = [
    "Bronze",
    "Silver",
    "Gold",
    "Platinum",
    "Diamond",
    "Crown",
    "Ace"
];
var prices = [
    [50000, 70000, 90000, 110000, 120000, 150000, 200000, 250000, 300000, 350000, 350000, 400000, 450000, 500000, 550000, 600000, 675000, 750000, 825000, 850000, 900000, 975000, 1050000, 1125000, 1200000, 1250000],
    [0, 50000, 50000, 70000, 75000, 100000, 150000, 200000, 250000, 275000, 300000, 350000, 400000, 450000, 500000, 550000, 625000, 700000, 775000, 825000, 850000, 925000, 1000000, 1075000, 1150000, 1200000],
    [0, 0, 25000, 50000, 50000, 70000, 125000, 175000, 225000, 250000, 275000, 325000, 375000, 425000, 475000, 500000, 600000, 675000, 750000, 800000, 850000, 925000, 1000000, 1075000, 1100000, 1175000],
    [0, 0, 0, 0, 50000, 70000, 125000, 175000, 225000, 250000, 250000, 300000, 350000, 400000, 450000, 475000, 575000, 650000, 700000, 750000, 825000, 900000, 975000, 1050000, 1075000, 1150000],
    [0, 0, 0, 0, 25000, 50000, 100000, 150000, 175000, 200000, 225000, 275000, 325000, 375000, 425000, 450000, 550000, 625000, 700000, 725000, 800000, 875000, 950000, 1000000, 1050000, 1125000],
    [0, 0, 0, 0, 0, 25000, 75000, 125000, 175000, 200000, 200000, 250000, 300000, 350000, 400000, 425000, 525000, 600000, 650000, 700000, 775000, 850000, 925000, 975000, 1025000, 1100000],
    [0, 0, 0, 0, 0, 0, 50000, 100000, 125000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 525000, 600000, 650000, 700000, 750000, 825000, 900000, 950000, 1000000, 1100000],
    [0, 0, 0, 0, 0, 0, 0, 50000, 100000, 125000, 150000, 200000, 250000, 300000, 350000, 400000, 475000, 550000, 625000, 650000, 700000, 775000, 850000, 925000, 950000, 1050000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 120000, 170000, 220000, 270000, 320000, 375000, 450000, 525000, 600000, 600000, 650000, 725000, 800000, 875000, 900000, 1000000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 150000, 200000, 250000, 300000, 350000, 425000, 500000, 525000, 550000, 625000, 700000, 775000, 850000, 875000, 950000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70000, 120000, 170000, 220000, 250000, 300000, 375000, 450000, 475000, 500000, 600000, 675000, 750000, 800000, 850000, 900000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 550000, 625000, 700000, 750000, 825000, 900000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 250000, 300000, 375000, 425000, 500000, 575000, 625000, 700000, 775000, 850000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 200000, 275000, 325000, 400000, 450000, 525000, 600000, 675000, 750000, 800000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 100000, 150000, 225000, 300000, 350000, 400000, 475000, 550000, 625000, 700000, 750000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000, 125000, 200000, 250000, 300000, 350000, 425000, 500000, 575000, 650000, 700000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 225000, 250000, 300000, 375000, 450000, 525000, 600000, 650000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 225000, 250000, 325000, 400000, 475000, 525000, 600000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 225000, 300000, 350000, 400000, 450000, 525000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75000, 150000, 200000, 275000, 350000, 425000, 475000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 175000, 225000, 300000, 375000, 400000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, , 0, 100000, 200000, 250000, 300000, 350000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 200000, 250000, 300000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 200000, 250000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000, 200000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100000],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
];

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    for (var i=0; i<ranks.length; i++) {
        $("#current-ranks").append("<option>"+ranks[i]+"</option>");
    }
    for (var i=1; i<ranks.length; i++) {
        $("#target-ranks").append("<option>"+ranks[i]+"</option>");
    }
    $("#current-ranks").change(function() {
        refreshPrice();
    });
    $("#target-ranks").change(function() {
        refreshPrice();
    });
    $("#current-tiers").change(function() {
        refreshPrice();
    });
    $("#target-tiers").change(function() {
        refreshPrice();
    });
});

function showRules() {
    rulesShown = !rulesShown;
    if (rulesShown) {
        $("#dropdown-img").css({
            "transform": "rotate(-180deg)"
        });
        $("#rules").css({
            "height": "470px"
        });
    } else {
        $("#dropdown-img").css({
            "transform": "rotate(0deg)"
        });
        $("#rules").css({
            "height": "100px"
        });
    }
}

function checkout() {
    var currentRank = $("#current-ranks").val();
    var currentTier = $("#current-tiers").val();
    //var currentStar = $("#current-stars").val();
    var targetRank = $("#target-ranks").val();
    var targetTier = $("#target-tiers").val();
    //var targetStar = $("#target-stars").val();
    var server = 0;
    if ($("#ios").prop("checked")) {
        server = 1;
    }
    var level = $("#level").val().trim();
    var heros = $("#heros").val().trim();
    var email = $("#email").val().trim();
    var password = $("#password").val();
    if (level == "") {
        alert("Mohon masukkan level");
        return;
    }
    if (heros == "") {
        alert("Mohon masukkan jumlah hero");
        return;
    }
    if (email == "") {
        alert("Mohon masukkan email Moonton");
        return;
    }
    if (password == "") {
        alert("Mohon masukkan password Moonton");
        return;
    }
    var agree = $("#checkbox").prop("checked");
    if (!agree) {
        alert("Mohon setujui syarat dan ketentuan");
        return;
    }
    refreshPrice();
    $("#checkout").prop("disabled", true);
    var fd = new FormData();
    fd.append("type", "PUBG");
    fd.append("current_rank", currentRank);
    fd.append("current_tier", currentTier);
    fd.append("target_rank", targetRank);
    fd.append("target_tier", targetTier);
    fd.append("server", server);
    fd.append("level", level);
    fd.append("heros", heros);
    fd.append("email", email);
    fd.append("password", password);
    $.ajax({
        type: 'POST',
        url: 'https://fdelivery.xyz/joki-mobile-legends/php/checkout.php',
        data: fd,
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
            alert("Pesanan selesai dibuat, kamu akan menerima email jika pesanan sudah dilakukan.");
            $("#current-ranks").prop("selectedIndex", 0);
            $("#current-tiers").prop("selectedIndex", 0);
            $("#target-ranks").prop("selectedIndex", 0);
            $("#target-tiers").prop("selectedIndex", 0);
            $("#level").val("");
            $("#heros").val("");
            $("#email").val("");
            $("#password").val("");
            $("#agree").prop("checked", false);
            $("#android").prop("checked", true);
            $("#checkout").prop("disabled", false);
            window.location.href = "http://wa.me/081280792311";
        }
    });
}

function refreshPrice() {
    var currentRank = $("#current-ranks").prop("selectedIndex");
    var currentTier = $("#current-tiers").prop("selectedIndex");
    var targetRank = $("#target-ranks").prop("selectedIndex");
    var targetTier = $("#target-tiers").prop("selectedIndex");
    var row = -1;
    if (currentRank == 0) {
        row = 0;
    } else if (currentRank == 1) {
        switch (currentTier) {
            case 4: row = 1; break;
            case 3: row = 2; break;
            case 2: row = 3; break;
            case 1: row = 4; break;
            case 0: row = 5; break;
        }
    } else if (currentRank == 2) {
        switch (currentTier) {
            case 4: row = 6; break;
            case 3: row = 7; break;
            case 2: row = 8; break;
            case 1: row = 9; break;
            case 0: row = 10; break;
        }
    } else if (currentRank == 3) {
        switch (currentTier) {
            case 4: row = 11; break;
            case 3: row = 12; break;
            case 2: row = 13; break;
            case 1: row = 14; break;
            case 0: row = 15; break;
        }
    } else if (currentRank == 4) {
        switch (currentTier) {
            case 4: row = 16; break;
            case 3: row = 17; break;
            case 2: row = 18; break;
            case 1: row = 19; break;
            case 0: row = 20; break;
        }
    } else if (currentRank == 5) {
        switch (currentTier) {
            case 4: row = 21; break;
            case 3: row = 22; break;
            case 2: row = 23; break;
            case 1: row = 24; break;
            case 0: row = 25; break;
        }
    } else if (currentRank == 6) {
        row = 26;
    }
    var col = -1;
    if (targetRank == 0) {
        switch (targetTier) {
            case 4: col = 0; break;
            case 3: col = 1; break;
            case 2: col = 2; break;
            case 1: col = 3; break;
            case 0: col = 4; break;
        }
    } else if (targetRank == 1) {
        switch (targetTier) {
            case 4: col = 5; break;
            case 3: col = 6; break;
            case 2: col = 7; break;
            case 1: col = 8; break;
            case 0: col = 9; break;
        }
    } else if (targetRank == 2) {
        switch (targetTier) {
            case 4: col = 10; break;
            case 3: col = 11; break;
            case 2: col = 12; break;
            case 1: col = 13; break;
            case 0: col = 14; break;
        }
    } else if (targetRank == 3) {
        switch (targetTier) {
            case 4: col = 15; break;
            case 3: col = 16; break;
            case 2: col = 17; break;
            case 1: col = 18; break;
            case 0: col = 19; break;
        }
    } else if (targetRank == 4) {
        switch (targetTier) {
            case 4: col = 20; break;
            case 3: col = 21; break;
            case 2: col = 22; break;
            case 1: col = 23; break;
            case 0: col = 24; break;
        }
    } else if (targetRank == 5) {
        col = 25;
    }
    console.log("Current rank: "+currentRank+", current tier: "+currentTier+", target rank: "+targetRank+", target tier: "+targetTier+", row: "+row+", col: "+col);
    var price;
    if (row == -1 || col == -1) {
        price = 0;
    } else {
        price = prices[row][col];
    }
    console.log("Price = "+price);
    $("#price").html("Rp"+formatMoney(price)+",-");
}